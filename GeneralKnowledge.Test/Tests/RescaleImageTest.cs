﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)
            string _filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            _filePath = Directory.GetParent(Directory.GetParent(_filePath).FullName).FullName;
            var file = Path.Combine(_filePath, @"Resources\car1.jpg");
            var desThumbFilePath = Path.Combine(_filePath, @"Resources\newimag.jpg");
            ImageResizer.ImageJob i = new ImageResizer.ImageJob(file, desThumbFilePath, new ImageResizer.ResizeSettings(
              "width= 100;height=80;format=jpg;mode=max"));
            i.CreateParentDirectory = true; //Auto-create the uploads directory.
            i.Build();

            var PreviewThumbFilePath = Path.Combine(_filePath, @"Resources\newPreviewimag.jpg");
            ImageResizer.ImageJob ip = new ImageResizer.ImageJob(file, PreviewThumbFilePath, new ImageResizer.ResizeSettings(
              "width= 1200;height=1600;format=jpg;mode=max"));
            ip.CreateParentDirectory = true; //Auto-create the uploads directory.
            ip.Build();
        }
    }
}
