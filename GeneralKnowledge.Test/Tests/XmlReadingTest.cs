﻿using GeneralKnowledge.Test.App.Common;
using GeneralKnowledge.Test.App.DomainModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test"; } }

        public void Run()
        {
            var xmlData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {
            XElement xelement = XElement.Parse(xml);
            IEnumerable<XElement> valuesTypes = xelement.Elements();
            var childElements = valuesTypes.Elements();
            var node = childElements.Nodes();
            List<decimal> temperature = new List<decimal>();
            List<decimal> pH = new List<decimal>();
            List<decimal> Chloride = new List<decimal>();
            List<decimal> Phosphate = new List<decimal>();
            List<decimal> Nitrate = new List<decimal>();
            foreach (var val in childElements)
            {
                switch (val.FirstAttribute.Value)
                {
                    case "temperature":
                        temperature.Add(decimal.Parse(val.Value));
                        break;
                    case "pH":
                        pH.Add(decimal.Parse(val.Value));
                        break;
                    case "Phosphate":
                        Phosphate.Add(decimal.Parse(val.Value));
                        break;
                    case "Chloride":
                        Chloride.Add(decimal.Parse(val.Value));
                        break;
                    case "Nitrate":
                        Nitrate.Add(decimal.Parse(val.Value));
                        break;
                }

            }
            Console.WriteLine("parameter   LOW  AVG  MAX");

            Console.WriteLine("Temperature " + temperature.Min() + "   " + temperature.Average() + "   " + temperature.Max());

            Console.WriteLine("Chloride " + Chloride.Min() + "             " + Chloride.Average() + "          " + Chloride.Max());
            Console.WriteLine("Nitrate " + Nitrate.Min() + "               " + Nitrate.Average() + "               " + Nitrate.Max());
            Console.WriteLine("Phosphate " + Phosphate.Min() + "           " + Phosphate.Average() + "                 " + Phosphate.Max());
            Console.WriteLine("pH " + pH.Min() + "   " + pH.Average() + "   " + pH.Max());


        }
    }
}
