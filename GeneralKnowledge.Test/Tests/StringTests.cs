﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic string manipulation exercises
    /// </summary>
    public class StringTests : ITest
    {
        public void Run()
        {
            // TODO:
            // Complete the methods below

            AnagramTest();
            GetUniqueCharsAndCount();
        }

        private void AnagramTest()
        {
            var word = "stop";
            var possibleAnagrams = new string[] { "test", "tops", "spin", "post", "mist", "step" };

            foreach (var possibleAnagram in possibleAnagrams)
            {
                Console.WriteLine(string.Format("{0} > {1}: {2}", word, possibleAnagram, possibleAnagram.IsAnagram(word)));
            }
        }

        private void GetUniqueCharsAndCount()
        {
            var word = "xxzwxzyzzyxwxzyxyzyxzyxzyzyxzzz";
            //since its specified algorithm havent used linq or any short cut methods.
            string output = CountAllChars(word);
            Console.WriteLine(output);
            Console.ReadLine();
            // TODO:
            // Write an algoritm that gets the unique characters of the word below 
            // and counts the number of occurences for each character found
        }

        public static string CountAllChars(string s)
        {
            if (s == null) return null;
            if (s == "") return "";
            s = s.ToLower();
            char[] chars = s.ToCharArray();
            Array.Sort(chars);
            StringBuilder sb = new StringBuilder();
            int count = 0;
            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] < 'a' || chars[i] > 'z') continue;
                if (sb.Length == 0)
                {
                    sb = sb.Append(chars[i]);
                    count = 1;
                }
                else if (chars[i] == chars[i - 1])
                {
                    count++;
                }
                else
                {
                    sb = sb.Append(count.ToString());
                    sb = sb.Append(chars[i]);
                    count = 1;
                }
            }
            sb = sb.Append(count.ToString());
            return sb.ToString();
        }
    }


}
public static class StringExtensions
{
    public static bool IsAnagram(this string first, string second)
    {
        if (first.Length != second.Length)
            return false;

        if (first == second)
            return true;
        var firstcharArray = first.ToCharArray();
        var secondcharArray = second.ToCharArray();
        Array.Sort(firstcharArray);
        Array.Sort(secondcharArray);
        for (int index = 0; index != firstcharArray.Length; ++index)
        {
            if (firstcharArray[index] != secondcharArray[index])
            {
                return false;
            }
        }
        return true;           
    }
}



