﻿using GeneralKnowledge.Test.App.DomainModels;
using GeneralKnowledge.Test.App.EFDataContext;
using GeneralKnowledge.Test.Domain;
using LinqToExcel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            // var csvFile = Resources.AssetImport;
            string _filePath = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory);
            _filePath = Directory.GetParent(Directory.GetParent(_filePath).FullName).FullName;
            var csvFile = Path.Combine(_filePath, @"Resources\AssetImport.csv");
            this.GetExcelRecord(csvFile);

        }
        public void GetExcelRecord(string pathToExcelFile)
        {
            string sheetName = "AssetImport";
            var excelFile = new ExcelQueryFactory(pathToExcelFile);
            var asetRecords = from a in excelFile.Worksheet<AssetExcelDomain>(sheetName) select a;
            PersistToDB(asetRecords);
        }

        public void PersistToDB(IQueryable<AssetExcelDomain> asetRecords)
        {

            //Parallel.ForEach(asetRecords, (a) =>
            //{
            //    using (var ctx1 = new AssetContext())
            //    {
            //        var existingMime = ctx1.Mime.Where(x => x.mime_type == a.mime_type).FirstOrDefault();
            //        var assets = new AssetDomain();
            //        if (existingMime == null)
            //        {
            //            var mime = new MimeTypeDomain();
            //            mime.mime_type = a.mime_type;
            //            ctx1.Mime.Add(mime);
            //            assets.Mime_Type = mime;
            //        }
            //        else
            //        {
            //            assets.Mime_Type = existingMime;
            //        }
            //        var File = new FileNameDomain();
            //        File.FileName = a.file_name;
            //        ctx1.File.Add(File);
            //        assets.File_Id = File;
            //        assets.asset_Id = a.asset_id;
            //        ctx1.Assets.Add(assets);
            //        try
            //        {
            //            ctx1.SaveChanges();
            //        }
            //        catch (Exception ex)
            //        {

            //        }
            //    }
            //}
            //   );


            using (var ctx = new AssetContext())
            {

                foreach (var a in asetRecords)
                {
                    var existingMime = ctx.Mime.Where(x => x.mime_type == a.mime_type).FirstOrDefault();
                    var assets = new AssetDomain();
                    if (existingMime == null)
                    {
                        var mime = new MimeTypeDomain();
                        mime.mime_type = a.mime_type;
                        ctx.Mime.Add(mime);
                        assets.Mime_Type = mime;
                    }
                    else
                    {
                        assets.Mime_Type = existingMime;
                    }
                    var File = new FileNameDomain();
                    File.FileName = a.file_name;
                    ctx.File.Add(File);
                    assets.File_Id = File;                    
                    assets.asset_Id = a.asset_id;
                    assets.country = a.country;
                    assets.Email = a.email;
                    assets.created_by = a.created_by;
                    ctx.Assets.Add(assets);
                }
                try
                {
                    ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                
            }
        }
    }
}


