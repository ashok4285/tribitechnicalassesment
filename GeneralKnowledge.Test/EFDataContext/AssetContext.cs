﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralKnowledge.Test.App.DomainModels;

namespace GeneralKnowledge.Test.App.EFDataContext
{
    public class AssetContext : DbContext
    {
        public AssetContext() : base("name=AssetModel")
        {

        }

        public DbSet<AssetDomain> Assets { get; set; }
        public DbSet<FileNameDomain> File { get; set; }
        public DbSet<MimeTypeDomain> Mime { get; set; }

    }
}
