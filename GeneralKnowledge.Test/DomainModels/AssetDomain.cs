﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DomainModels
{
    public class AssetDomain
    {
        [Key]
        public string asset_Id { get; set; }

        public MimeTypeDomain Mime_Type { get; set; }
        public FileNameDomain File_Id { get; set; }

        public string created_by { get; set; }
        public string description { get; set; }

        public string country { get; set; }
        public string Email { get; set; }


    }
}
