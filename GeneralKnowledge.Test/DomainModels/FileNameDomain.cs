﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DomainModels
{
   public class FileNameDomain
    {
        [Key]
        public int File_id { get; set; }
        public string FileName { get; set; }
    }
}
