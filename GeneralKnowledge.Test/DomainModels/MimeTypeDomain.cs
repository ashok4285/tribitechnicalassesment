﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.DomainModels
{
   public class MimeTypeDomain
    {
        [Key]
        public int mime_id { get; set; }
        public string mime_type { get; set; }
        public ICollection<AssetDomain> Assets { get; set; }
    }
}
