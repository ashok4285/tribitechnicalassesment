namespace Assets.Repository
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Assets.Domain;

    public partial class AssetModel : DbContext
    {
        public AssetModel()
            : base("name=AssetModel")
        {
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
        public DbSet<AssetDomain> AssetInfoes { get; set; }
    }
}
