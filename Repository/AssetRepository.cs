﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Domain;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;

namespace Assets.Repository
{
    public class AssetRepository
    {
        //private AssetModel db;
        //public AssetRepository()
        //{
        //    db = new AssetModel();
        //}

        // GET api/EmployeeInfoAPI
        public IEnumerable<AssetDomain> GetEmployeeInfoes()
        {
            using (var ctx = new AssetModel())
            {
                if (ctx.AssetInfoes.Count() < 1)
                {
                    var AssetOneValue = new AssetDomain();
                    AssetOneValue.Asset_ID = "51df6a98-614e-40ef-8885-95ae50940058";
                    AssetOneValue.Country = "United States";
                    AssetOneValue.Mime_Type = "application/x-authorware-map";
                    AssetOneValue.Created_By = "sblack0";
                    AssetOneValue.Description = "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.";
                    AssetOneValue.Email = "jmitchell0@huffingtonpost.com";
                    AssetOneValue.File_Name = "ElitProin.aam";
                    ctx.AssetInfoes.Add(AssetOneValue);
                    ctx.SaveChanges();
                }
                var model = ctx.AssetInfoes.AsNoTracking().ToList();
                return model;
            }
        }



        public AssetDomain GetAssetInfoes(string ID)
        {
            using (var ctx = new AssetModel())
            {
                return ctx.AssetInfoes.AsNoTracking().Where(x => x.Asset_ID == ID).FirstOrDefault();
            }
        }

        public AssetDomain SaveAssetInfoes(AssetDomain domain)
        {
            using (var ctx = new AssetModel())
            {
                ctx.AssetInfoes.Add(domain);
                ctx.SaveChanges();
            }
            return domain;
        }


        public AssetDomain UpdateAssetInfo(AssetDomain domain)
        {
            AssetDomain assetinfo;
            using (var ctx = new AssetModel())
            {
                assetinfo = ctx.AssetInfoes.Where(x => x.Asset_ID == domain.Asset_ID).FirstOrDefault();
                assetinfo.Country = domain.Country;
                assetinfo.Created_By = domain.Created_By;
                assetinfo.Description = domain.Description;
                assetinfo.Email = domain.Email;
                assetinfo.File_Name = domain.File_Name;
                assetinfo.Mime_Type = domain.Mime_Type;
                ctx.Entry(assetinfo).State = EntityState.Modified;                
                ctx.SaveChanges();
            }
            return domain;
        }

        public bool DeleteAssetInfo(AssetDomain domain)
        {
            using (var ctx = new AssetModel())
            {
                AssetDomain assetinfo = ctx.AssetInfoes.Where(x => x.Asset_ID == domain.Asset_ID).FirstOrDefault();
                ctx.AssetInfoes.Remove(assetinfo);
                ctx.SaveChanges();
            }
            return true;

        }
    }
}
