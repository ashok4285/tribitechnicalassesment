﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Assets.Centric;
using Assets.Manager;
namespace AppServices.Controllers
{
    [RoutePrefix("api/assets")]
    public class ValuesController : ApiController
    {
        private IAssetsManager AssetsManager;
        public ValuesController()
        {
            var container = IOCContainer.Current;
            var managerFactory = container.GetInstance<IManagerFactory>();
            this.AssetsManager = managerFactory.GetInstance<IAssetsManager>();
        }
        // GET api/values
        [HttpGet]
        [Route("GetAllAssets")]
        public HttpResponseMessage GetAllAssets()
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.AssetsManager.GetAllAssetsDetails());
           
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
