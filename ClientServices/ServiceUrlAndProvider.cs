﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Asset.ClientServices
{
  public  class ServiceUrlAndProvider
    {
        public string get(string URL)
        {
            string result = SendRequest(URL, HttpMethod.Get);
            return result;
        }

        public string post(string URL,string data)
        {
            string result = SendRequest(URL, HttpMethod.Post,data);
            return result;
        }

        public string Put(string URL, string data)
        {
            string result = SendRequest(URL, HttpMethod.Put, data);
            return result;
        }

        public string Delete(string URL, string data)
        {
            string result = SendRequest(URL, HttpMethod.Delete, data);
            return result;
        }

        private string SendRequest(string URL, HttpMethod httpMethod,string Json ="")
        {
           
            string Output = string.Empty;
            HttpResponseMessage response = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(URL),
                    Method = httpMethod,
                };
                if (Json != "")
                {
                    request.Content = new StringContent(Json, Encoding.UTF8, "application/json");
                }
                response = httpClient.SendAsync(request).GetAwaiter().GetResult();
                Stream OutputData = response.Content.ReadAsStreamAsync().GetAwaiter().GetResult();
                StreamReader _OutputData = new StreamReader(OutputData);
                Output = _OutputData.ReadToEnd();
            }
            return Output;
        }
    }
}
