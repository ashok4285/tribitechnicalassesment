﻿using Assests.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asset.ClientServices
{
   public interface IAssets
    {
        IEnumerable<AssetsContract> GetAllAssetDetails(string url);

        AssetsContract SaveAssetDetails(string url, AssetsContract contract);
        AssetsContract UpdateAssetDetails(string url, AssetsContract contract);
        bool DeleteAssetDetails(string url, AssetsContract contract);

    }
}
