﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assests.DataContracts;
using Newtonsoft.Json;

namespace Asset.ClientServices
{
    public class AssetClientServices : IAssets
    {
        public bool DeleteAssetDetails(string url, AssetsContract contract)
        {
            ServiceUrlAndProvider service = new ServiceUrlAndProvider();
            var Json = service.Delete(url , JsonConvert.SerializeObject(contract));
            return true;
        }

        public IEnumerable<AssetsContract> GetAllAssetDetails(string url)
        {
            ServiceUrlAndProvider service = new ServiceUrlAndProvider();
            var Json = service.get(url);
            var lstAssets = JsonConvert.DeserializeObject<List<AssetsContract>>(Json);
            return lstAssets;
        }       

        public AssetsContract SaveAssetDetails(string url, AssetsContract contract)
        {
            ServiceUrlAndProvider service = new ServiceUrlAndProvider();
            Guid g;
            g = Guid.NewGuid();
            contract.Asset_ID = g.ToString();
            var jsonString = JsonConvert.SerializeObject(contract);
            var Json = service.post(url, jsonString);
            var lstAssets = JsonConvert.DeserializeObject<AssetsContract>(Json);
            return lstAssets;
        }

        public AssetsContract UpdateAssetDetails(string url, AssetsContract contract)
        {
            ServiceUrlAndProvider service = new ServiceUrlAndProvider();
            var jsonString = JsonConvert.SerializeObject(contract);
            var Json = service.Put(url, jsonString);
            var lstAssets = JsonConvert.DeserializeObject<AssetsContract>(Json);
            return lstAssets;
        }
    }
}
