﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace Assets.Domain
{
    public class AssetDomain
    {
        [Key]
        public string Asset_ID { get; set; }
        public string File_Name { get; set; }
        public string Mime_Type { get; set; }
        public string Created_By { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
    }
}
