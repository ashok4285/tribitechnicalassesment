﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Asset.ClientServices;
using Assests.DataContracts;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Assets.Centric;


namespace WebExperience.Test.Controllers
{
    [System.Web.Http.RoutePrefix("api/assets")]
    public class AssetController : ApiController
    {
        // TODO:
        // Create an API controller via REST to perform all CRUD operations on the asset objects created as part of the CSV processing test
        // Visualize the assets in a paged overview showing the title and created on field
        // Clicking an asset should navigate the user to a detail page showing all properties
        // Any data repository is permitted
        // Use a client MVVM framework
        private string URl;
        public AssetController()
        {
            URl = ConfigurationManager.AppSettings["ServiceUrl"];
        }

      

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("GetAllAssets")]
        public HttpResponseMessage GetAllAssetDetails()
        {
            AssetClientServices service = new AssetClientServices();
            var serviceUrl = URl + "/" + UrlConstants.GetAllAssetUrl;
            var result = service.GetAllAssetDetails(serviceUrl);           
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("SaveAsset")]
        public HttpResponseMessage SaveAssetDetails(AssetsContract contract)
        {
            AssetClientServices service = new AssetClientServices();
            var serviceUrl = URl + "/" + UrlConstants.SaveAssetUrl;
            var result = service.SaveAssetDetails(serviceUrl, contract);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("UpdateAsset")]
        public HttpResponseMessage UpdateAsset(AssetsContract contract)
        {
            AssetClientServices service = new AssetClientServices();
            var serviceUrl = URl + "/" + UrlConstants.UpdateAssetUrl;
            var result = service.UpdateAssetDetails(serviceUrl,contract);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }


        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("DeleteAsset")]
        public HttpResponseMessage DeleteAsset(AssetsContract contract)
        {
            AssetClientServices service = new AssetClientServices();
            var serviceUrl = URl + "/" + UrlConstants.DeleteAssetUrl;
            var result = service.DeleteAssetDetails(serviceUrl, contract);
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }    


    }
}
