﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Centric
{
  public static  class UrlConstants
    {
        public const string GetAllAssetUrl = "assets/GetAllAssets";
        public const string SaveAssetUrl = "assets/SaveAsset";
        public const string UpdateAssetUrl = "assets/UpdateAsset";
        public const string DeleteAssetUrl = "assets/DeleteAsset";
    }
}
