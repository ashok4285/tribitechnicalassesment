﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleInjector;
namespace Assets.Centric
{
    public class IOCContainer
    {
        private static Container iocContainer;

        public static Container Current
        {
            get
            {
                if (iocContainer == null)
                {
                    iocContainer = new Container();
                }

                return iocContainer;
            }
        }

        public static void Reset()
        {
            iocContainer = new Container();
        }
    }
}
