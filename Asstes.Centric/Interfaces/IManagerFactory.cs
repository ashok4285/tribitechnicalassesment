﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Centric
{
    public interface IManagerFactory
    {
        TManager GetInstance<TManager>() where TManager : class;
    }

}
