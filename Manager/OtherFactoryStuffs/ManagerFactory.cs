﻿using Assets.Centric;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Manager
{
    public sealed class ManagerFactory : IManagerFactory
    {
        private Container container;

        public ManagerFactory()
        {
            this.container = IOCContainer.Current;
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <typeparam name="TRepository">The type of the repository.</typeparam>
        /// <returns></returns>
        public TManager GetInstance<TManager>() where TManager : class
        {
            // this.CreateUow();
            var manager = this.container.GetInstance<TManager>();
            return manager;
        }

        public static void Register()
        {
            var container = IOCContainer.Current;
            //  var container = IOCContainer.Current;
            container.Register<IAssetsManager, AssetManager>(Lifestyle.Transient);
          
        }

        public static void Intialize()
        {
            var container = IOCContainer.Current;
            container.Register<IManagerFactory, ManagerFactory>(Lifestyle.Transient);
        }
    }
}
