﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Domain;
using Assests.DataContracts;


namespace Assets.Manager
{
   public interface IAssetsManager
    {
        IEnumerable<AssetsContract> GetAllAssetsDetails();
        AssetsContract SaveAsset(AssetsContract contract);

        AssetDomain GetAssetById(string Asset_ID);
        AssetsContract UpdateAsset(AssetsContract contract);
        bool DeleteAsset(AssetsContract contract);

        AssetDomain AdaptToDomain( AssetsContract contract, AssetDomain domain);
        AssetsContract AdaptToContract(AssetDomain domain);
    }
}
