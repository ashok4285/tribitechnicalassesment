﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assests.DataContracts;
using Assets.Domain;
using Assets.Repository;
namespace Assets.Manager
{
    public class AssetManager : IAssetsManager
    {
        public AssetsContract AdaptToContract(AssetDomain domain)
        {
            return new AssetsContract()
            {
                Asset_ID = domain.Asset_ID,
                Country = domain.Country,
                File_Name = domain.File_Name,
                Mime_Type = domain.Mime_Type,
                Description = domain.Description,
                Created_By = domain.Created_By,
                Email = domain.Email

            };
        }

        public AssetDomain AdaptToDomain(AssetsContract contract, AssetDomain domain)
        {
            domain.Asset_ID = contract.Asset_ID;
            domain.Country = contract.Country;
            domain.File_Name = contract.File_Name;
            domain.Mime_Type = contract.Mime_Type;
            domain.Description = contract.Description;
            domain.Created_By = contract.Created_By;
            domain.Email = contract.Email;
            return domain;
        }

        public bool DeleteAsset(AssetsContract contract)
        {
            var domainObj = this.GetAssetById(contract.Asset_ID);
            domainObj = this.AdaptToDomain(contract, domainObj);
            AssetRepository repos = new AssetRepository();
            repos.DeleteAssetInfo(domainObj);
            return true;         
        }

        public IEnumerable<AssetsContract> GetAllAssetsDetails()
        {

            AssetRepository repos = new AssetRepository();
            var domainobj = repos.GetEmployeeInfoes();
            var contracts = new List<AssetsContract>();
            foreach (var domainob in domainobj)
            {
                contracts.Add(this.AdaptToContract(domainob));
            }
            return contracts;
            //var AssetOne = new AssetDomain();
            //AssetOne.Asset_ID = "51df6a98-614e-40ef-8885-95ae50940058";
            //AssetOne.Country = "United States";
            //AssetOne.Mime_Type = "application/x-authorware-map";
            //AssetOne.Created_By = "sblack0";
            //AssetOne.Description = "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.";
            //AssetOne.Email = "jmitchell0@huffingtonpost.com";
            //AssetOne.File_Name = "ElitProin.aam";
            //var AssetDetails = new List<AssetDomain>();
            //AssetDetails.Add(AssetOne);
            //return val;
        }

        public AssetDomain GetAssetById(string Asset_ID)
        {
            AssetRepository repos = new AssetRepository();
            return repos.GetAssetInfoes(Asset_ID);
        }

        public AssetsContract SaveAsset(AssetsContract contract)
        {
            var domainobj = new AssetDomain();
            domainobj = this.AdaptToDomain(contract, domainobj);
            AssetRepository repos = new AssetRepository();
            repos.SaveAssetInfoes(domainobj);
            return this.AdaptToContract(domainobj);

        }

        public AssetsContract UpdateAsset(AssetsContract contract)
        {
            var domainObj = this.GetAssetById(contract.Asset_ID);
            domainObj = this.AdaptToDomain(contract, domainObj);
            AssetRepository repos = new AssetRepository();
            repos.UpdateAssetInfo(domainObj);
            return this.AdaptToContract(domainObj);

        }


    }
}
