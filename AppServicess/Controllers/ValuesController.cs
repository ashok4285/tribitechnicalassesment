﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Assets.Centric;
using Assets.Manager;
using Assests.DataContracts;
namespace AppServicess.Controllers
{
    [RoutePrefix("api/assets")]
    public class ValuesController : ApiController
    {
        private IAssetsManager AssetsManager;
        public ValuesController()
        {
            var container = IOCContainer.Current;
            var managerFactory = container.GetInstance<IManagerFactory>();
            this.AssetsManager = managerFactory.GetInstance<IAssetsManager>();
        }
        // GET api/values
        [HttpGet]
        [Route("GetAllAssets")]
        public HttpResponseMessage GetAllAssets()
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.AssetsManager.GetAllAssetsDetails());
           
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        [Route("SaveAsset")]
        public HttpResponseMessage Post(AssetsContract contract)
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.AssetsManager.SaveAsset(contract));
        }

        // PUT api/values/5
        [HttpPut]
        [Route("UpdateAsset")]
        public HttpResponseMessage Put(AssetsContract contract)
        {
            return Request.CreateResponse(HttpStatusCode.OK, this.AssetsManager.UpdateAsset(contract));
        }

        [HttpDelete]
        [Route("DeleteAsset")]
        public HttpResponseMessage Delete(AssetsContract contract)
        {
           
           return Request.CreateResponse(HttpStatusCode.OK, this.AssetsManager.DeleteAsset(contract));
            //return Request.CreateResponse(HttpStatusCode.OK, true);
        }
    }
}
